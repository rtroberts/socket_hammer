package socketio

import (
	"errors"
	//"fmt"
	"strconv"
	"strings"
)

type Message struct {
	Type int
	//ID       string
	//Endpoint *Endpoint
	Data string
}

// ParseMessages parses the given raw message in to Message. NOTE: See stackoverflow
// post below, taken from: http://stackoverflow.com/questions/23987640/socket-io-handshake-return-error-transport-unknown
func parseMessage(rawMsg string) (*Message, error) {
	parts := strings.SplitN(rawMsg, "[", 4)
	//"42[\"lineups\",{\"message\"":
	//fmt.Println(rawMsg)
	if len(parts) < 1 {
		return nil, errors.New("Empty message")
	}

	msgType, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, err
	}
	data := ""

	if len(parts) > 1 {
		if parts[1] != "" {
			data = parts[1]
		}
	}
	//if parts[3] != "" {

	//	data := ""
	//	if len(parts) == 4 {
	//		data = parts[3]
	//	}
	//}
	return &Message{msgType, data}, nil
}

// String returns the string represenation of the Message.
func (m Message) String() string {
	raw := strconv.Itoa(m.Type)

	//	raw += ":" + m.ID
	//
	//	raw += ":"
	//	if m.Endpoint != nil {
	//		raw += m.Endpoint.String()
	//	}
	//
	if m.Data != "" {
		raw += ":" + m.Data
	}

	return raw
}

// NewDisconnect returns a new disconnect Message.
func NewDisconnect() *Message {
	return &Message{Type: 0, Data: ""}
}

// NewConnect returns a new connect Message for the given endpoint.
func NewConnect(endpoint *Endpoint) *Message {
	return &Message{Type: 52, Data: ""}
	//, Endpoint: endpoint}
}

// NewHeartbeat returns a new heartbeat Message.
func NewHeartbeat() *Message {
	return &Message{Type: 22, Data: ""}
}

//This isn't really implemented
func NewMessageMsg() *Message {
	return &Message{Type: 3, Data: ""}
}

func NewJSONMessage(data string) *Message {
	return &Message{Type: 42, Data: data}
}

func NewEvent() *Message {
	return &Message{Type: 5, Data: ""}
}

func NewACK() *Message {
	return &Message{Type: 6, Data: ""}
}

// NewError returns a new error Message for the given endpoint with a
// reason and an advice.
func NewError(data string) *Message {
	return &Message{Type: 7, Data: data}
}

// NewNoop returns a new no-op Message.
func NewNoop() *Message {
	return &Message{Type: 8}
}

/*
v1.0 changed its message transport format to engine.io_type + socket.io_type + real_message_in_json. (I don't know where endpoint in v0.9 kicks in.)

engine.io message type:

open =0
close =1
ping =2
pong =3
message =4
upgrade =5
noop =6
socket.io message type:

connect = 0
disconnect = 1
event = 2
ack = 3
error = 4
binary_event = 5
binary_ack = 6
So, "52" means UPGRADE_EVENT and "40" is MESSAGE_CONNECT. Typically, server messages start with "42", which is MESSAGE_EVENT. PING and PONG don't require a socket.io message. I wonder if UPGRADE works like that too.

*/

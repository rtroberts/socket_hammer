package socketio

import (
	"errors"
	//"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Session holds the configuration variables received from the socket.io
// server.
type Session struct {
	ID                 string
	SupportedProtocols []string
	HeartbeatTimeout   time.Duration
	ConnectionTimeout  time.Duration
}

// NewSession receives the configuration variables from the socket.io
// server.
func NewSession(url string) (*Session, error) {
	urlParser, err := newURLParser(url)
	if err != nil {
		return nil, err
	}

	response, err := http.Get(urlParser.handshake())
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	response.Body.Close()

	sessionVars := strings.Split(string(body[5:]), ":")

	//for i, val := range sessionVars {
	//	fmt.Printf("SessionVars Index: %v, val: %v\n", i, val)
	//}

	if len(sessionVars) != 5 {
		return nil, errors.New("Session variables is not 5")
	}

	// Strip " from the ID string
	idString := strings.Split(string(sessionVars[1]), ",")[0]
	id := idString[1 : len(idString)-1]
	//fmt.Printf("ID: %v\n", id) //DEBUG

	supportedProtocols := strings.Split(string(sessionVars[2]), ",")
	//fmt.Printf("SupportedProtocols: %v\n", supportedProtocols) //DEBUG
	heartbeatTimeoutSec, _ := strconv.Atoi(strings.Split(string(sessionVars[3]), ",")[0])
	//fmt.Printf("heartbeat: %v\n", heartbeatTimeoutSec) //DEBUG
	trimmed := sessionVars[4][:len(sessionVars[4])-1]
	connectionTimeoutSec, _ := strconv.Atoi(strings.Split(string(trimmed), ",")[0])
	//connectionTimeoutSec, _ := strconv.Atoi(strings.Split(string(sessionVars[4]), ",")[0])[:len(sessionVars[4])-1]
	//fmt.Printf("timeout: %v\n", connectionTimeoutSec) //DEBUG

	heartbeatTimeout := time.Duration(heartbeatTimeoutSec) * time.Millisecond
	//fmt.Println("HeartbeatTimeout session value", heartbeatTimeout)
	connectionTimeout := time.Duration(connectionTimeoutSec) * time.Millisecond
	//fmt.Println("supportedProtocols: ", supportedProtocols)
	return &Session{id, supportedProtocols, heartbeatTimeout, connectionTimeout}, nil
}

// SupportProtocol checks if the given protocol is supported by the
// socket.io server.
func (session *Session) SupportProtocol(protocol string) bool {
	for _, supportedProtocol := range session.SupportedProtocols {
		if protocol == supportedProtocol {
			return true
		}
	}
	return false
}
